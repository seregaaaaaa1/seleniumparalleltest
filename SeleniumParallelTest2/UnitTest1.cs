﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace SeleniumParallelTest2
{
    [TestFixture]
    [Parallelizable]
    public class InternetExplorertesting : Hooks
    {
        public InternetExplorertesting():base(BrowserType.IE)
        {

        }
        [Test]
        public void IEGoogleTest()
        {
            Driver.Navigate().GoToUrl("http://google.com");
            Driver.FindElement(By.Name("q")).SendKeys("Selenium");

            Driver.FindElement(By.Name("btnK")).Click();
            Assert.That(Driver.PageSource.Contains("Selenium"), Is.EqualTo(true),
                                                    "The text selenium soesn't exist");

        }
    }

    //[TestFixture]
    //[Parallelizable]
    //public class Chrometesting : Hooks
    //{
    //    public Chrometesting() : base(BrowserType.Chrome)
    //    {

    //    }
    //    [Test]
    //    public void ChromeGoogleTest()
    //    {
    //        //IWebDriver Driver = new ChromeDriver();
    //        Driver.Navigate().GoToUrl("http://google.com");
    //        // IWebElement element = Driver.FindElement(By.Name("q"));
    //        Driver.FindElement(By.Name("q")).SendKeys("ExecuteAutomation");
    //        Driver.FindElement(By.Name("btnK")).Click();
    //        //element.SendKeys("ExecuteAutomation");
    //        //  element.Submit();
    //        Assert.That(Driver.PageSource.Contains("ExecuteAutomation"), Is.EqualTo(true),
    //                                                "The text selenium soesn't exist");

    //    }
    //}
}
