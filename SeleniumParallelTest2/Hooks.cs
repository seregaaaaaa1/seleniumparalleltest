﻿using NUnit.Framework;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;

namespace SeleniumParallelTest2
{
    public enum BrowserType
    {
        Chrome,
        Firefox,
        IE
    }
    [TestFixture]
    public class Hooks:Base
    {
       private BrowserType _browsertype;

       public Hooks(BrowserType browser)
        {
            // Driver = new InternetExplorerDriver();
            //Driver = new ChromeDriver();
            _browsertype = browser;
        }

        [SetUp]
        public void InitializeTest()
        {
            ChooseDriverInstance(_browsertype);
        }
        public void ChooseDriverInstance(BrowserType browserType)
        {
            if (browserType == BrowserType.Chrome)
                Driver = new ChromeDriver();
            else if (browserType == BrowserType.IE)
            {
                Driver = new InternetExplorerDriver();
            }
        }
    }
}
